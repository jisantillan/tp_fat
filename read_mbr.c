#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE * in = fopen("test.img", "rb");
    unsigned int i, start_sector, length_sectors;
    
    fseek(in, 446 , SEEK_SET); // Voy al inicio. Completar donde dice ...
    
    for(i=0; i<4; i++) { // Leo las entradas
        //El %02X significa imprimir al menos 2 dígitos, anteponerlo con 0 si hay menos.
        printf("Partition entry %d: First byte (Bootable Flag) %02X\n", i, fgetc(in));
        printf("  Comienzo de partición en CHS: %02X:%02X:%02X\n", fgetc(in), fgetc(in), fgetc(in));
        unsigned char t_particion = fgetc(in);
        printf("  Partition type (0x%02X)", t_particion);
   
        switch (t_particion)
		{
		case 0x01:
			printf("  FAT12 \n");
			break;
		case 0x04:
			printf("  FAT16, 16-32 MB  \n");
			break;
		case 0x06:
			printf("  FAT16, 32 MB-2 GB  \n");
			break;
		case 0x07:
			printf("  NTFS \n");
			break;
		case 0x0B:
			printf("  FAT32 \n");
			break;
		case 0x0C:
			printf("  FAT32 LBA \n");
			break;
		case 0x0E:
			printf("  FAT16, 32 MB-2 GB, LBA  \n");
			break;
		case 0x83:
			printf("  Linux  \n");
			break;
			 
		default:
			printf("  Particion Vacia\n");
		}
		
        printf("  Fin de partición en CHS: %02X:%02X:%02X\n", fgetc(in), fgetc(in), fgetc(in));
        
        fread(&start_sector, 4, 1, in);
        fread(&length_sectors, 4, 1, in);
        //El %08X significa imprimir al menos 8 dígitos, anteponerlo con 0 si hay menos.
        printf("  Dirección LBA relativa 0x%08X, de tamaño en sectores %d\n", start_sector, length_sectors);
        printf("***************************************************************\n");
    }
    
    fclose(in);
    return 0;
}
