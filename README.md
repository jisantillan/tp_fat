Analisis de filesystem FAT12 a partir de una imagen iso

*********************************************************************
******************* PARA VER NUESTRO FILE MONTADO *******************
*********************************************************************
*sudo mount test.img /mnt -o loop,umask=000
Result: mount: /mnt: ATENCIÓN: el dispositivo está protegido contra 
escritura; se monta como sólo lectura.
*********************************************************************
*sudo umount /mnt
Result: desmontado ok.
*********************************************************************
******************** EJECUCION DE NUESTRO CODIGO ********************
*********************************************************************
Ejecucion de read_boot:
gcc read_boot.c -o read_boot
./read_boot
*********************************************************************
Ejecucion de read_boot:
gcc read_mbr.c -o read_mbr
./read_mbr
*********************************************************************
Ejecucion de read_root:
gcc read_root.c -o read_root
./read_root
*********************************************************************
Ejecucion de read_file:
gcc read_file.c -o read_file
./read_file
*********************************************************************
Ejecucion de recover_file:
gcc recover_file.c -o recover_file
./recover_file
*********************************************************************
***************************** LIMPIANDO *****************************
*********************************************************************
make clean -> Limpia todo lo generado de gcc xxxxx.c -o xxxxx
*********************************************************************
